export enum EventStatus {
  returned,
  lending,
  error
}

export class Event {
  id!: string
  member: string
  book: string
  lendingDate: Date
  returnDate?: Date
  fine?: number
  status: number = EventStatus.returned

  constructor ( member: string, book: string, lendingDate: Date = new Date()) {
    this.member = member
    this.book = book
    this.lendingDate = lendingDate
    this.status = EventStatus.lending
  }
}