import { Database, DatabaseFactory } from "./database";
import { Book } from "./book";

export interface Checker {
  //return book id list
  check_lend(book: string): Promise<Book[]>
  //return event id list
  check_return(book: string): Promise<Book[]>
}

export function CheckerFactory(db: Database): Checker {
  return new CheckerImpl(db)
}

class CheckerImpl implements Checker {
  private db: Database
  constructor (db: Database) {
    this.db = db
  }
  async check_lend(book_query: string): Promise<Book[]> {
    return this.db.get<Book>('book', {$or: {
      name: book_query,
      ISBN: book_query,
    }})
  }
  async check_return(book_query: string): Promise<Book[]> {
    return this.db.get<Book>('book', {$or: {
      name: book_query,
      ISBN: book_query,
    }, status: 1})
  }
}