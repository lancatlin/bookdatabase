export enum BookStatus {
  inside,
  outside,
  error 
}

export class Book {
  id!: string
  name: string
  title?: string[]
  publisher?: string
  year?: string
  author?: string
  ISBN?: string
  //捐贈者
  donor?: string
  status: number = BookStatus.inside
  classification?: number

  constructor ( name: string, author?: string, isbn?: string, 
    publisher?: string, classsfication?: number, title?: string[], donor?: string, year?: string) {
    this.name = name
    this.author = author
    this.ISBN = isbn
    this.publisher = publisher
    this.classification = classsfication
    this.title = title
    this.donor = donor
    this.year = year
  }
}