/**系統運作核心
 * 借書 / 還書操作
 * 查閱資料（尚未實做）
 */
import { Book, } from "./book";
import { Member, } from "./member";
import { Event, } from "./event"
import { Database, } from "./database";

//環境參數
const _fine: number = 10

/**借還書結果物件 */
export class Result {
  book: string
  member: string
  lendingDate: Date
  expireDate?: Date
  status: number
  fine?: number
  returnDate?: Date
  overdueTime?: number
  event !:Event

  constructor (book: string, member: string, event: Event, lendingDate: Date, status: number, 
    expireDate?: Date, fine?: number, returnDate?: Date, overdueTime?: number) {
      this.book = book
      this.member = member
      this.event = event
      this.lendingDate = lendingDate
      this.status = status
      if (expireDate) this.expireDate = expireDate
      if (fine) this.fine = fine
      if (returnDate) this.returnDate = returnDate
      if (overdueTime) this.overdueTime = overdueTime
    }
}

/**核心界面 */
export interface Core {
  //借書，輸入書籍/會員資料，返回還書日期等結果
  lend (book_id: string, member_id: string): Promise<Result>
  returnBook (book_id: string): Promise<Result>
  returnById (evnet_id: string): Promise<Result>
}

/**核心建構子 */
export function CoreFactory (db: Database) {
  return new CoreImpl(db)
}

/**將日期加上指定天數 */
function dateAdd (date: Date, day: number): Date {
  let result: Date = new Date
  result.setDate(date.getDate() + day)
  return result
}

/**核心實做 */
class CoreImpl implements Core {
  constructor (private db: Database) {}

  /**借書 */
  async lend (book_id: string, member_id: string): Promise<Result> {
    let book: Book = await this.db.getOne<Book>('book', {id: book_id})
    let member: Member = await this.db.getOne<Member>('member', {id: member_id})
    let event: Event = await this.db.insert<Event>('event', new Event(member.id, book.id))
    await this.db.update<Book>('book', {id: book.id}, {status: 1})
    await this.db.update<Member>('book', {id: member.id}, {status: 1})
    return new Result(
      book.name, member.name, event, event.lendingDate, 1, dateAdd(event.lendingDate, 30)
    )
  }

  /**還書 by 書籍ID */
  async returnBook (book_id: string): Promise<Result> {
    let book = await this.db.getOne<Book>('book', {id: book_id, status: 1})
    let event = await this.db.getOne<Event>('event', {status: 1, book: book.id})
    return await this.returning(event)
  }

  /**還書 by 事件ID */
  async returnById (event_id: string): Promise<Result> {
    let event = await this.db.getOne<Event>('event', {id: event_id})
    return await this.returning(event)
  }

  /**還書後端實做 */
  private async returning (event: Event): Promise<Result> {
    let today = new Date()
    let expireDate = dateAdd(event.lendingDate, 30)
    let overdueTime: number = Math.max(Math.ceil(
      (today.getTime() - expireDate.getTime()) / (1000*60*60*24)), 0)
    let fine: number = overdueTime * _fine
    event.fine = fine
    event.returnDate = today
    event.status = 0
    await this.db.updateObj('event', {id: event.id}, event)
    let book: Book = (await this.db.update<Book>('book', {id: event.book}, {status: 0}))[0]
    //member 須加入判斷還有幾本書
    let member: Member = (await this.db.update<Member>('member', {id: event.member}, {status: 0}))[0]
    return new Result(
      book.name,
      member.name,
      event,
      event.lendingDate,
      0,
      expireDate,
      fine,
      today,
      overdueTime
    )
  }
}