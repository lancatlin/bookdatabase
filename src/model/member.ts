enum education {
  junior,
  senior, 
  university, 
  master, 
  doctor
}

enum category {
  member,
  membership
}

export enum MemberStatus {
  unlending,
  lending,
  error
}

export class Member {
  id!: string
  phone?: string
  address?: string
  email?: string
  job?:  string
  education?: education
  recommender?: Member
  status: MemberStatus = MemberStatus.unlending

  constructor ( public name: string,
    public idnumber: string, public joinDate: Date,
    public category: number ) {}
}