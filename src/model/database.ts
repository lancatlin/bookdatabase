import { readFileSync, writeFileSync } from "fs";
import { debug } from "util";

const file_read: string = 'data_test.json'
const file_write: string = 'data.json'

/**Interface of Database */
export interface Database {
  insert<T extends { id: string }>(type: string, query: T): Promise<T>
  update<T>(type: string, query: any, newObj: any): Promise<T[]>
  updateObj<T>(type: string, query: any, newObj: T): Promise<T[]>
  //updateOne <T extends Struct> (type: string, query: any, newObj: T): Promise<T>
  get<T>(type: string, query: any): Promise<T[]>
  getOne<T>(type: string, query: any): Promise<T>
  delete<T>(type: string, query: any): Promise<T[]>
  //deleteOne <T extends Struct> (type: string, query: any): Promise<T>
  hash(type: string): string
}

/**Factory of Database */
export function DatabaseFactory(): Database {
  return new JSONDatabase()
}

class JSONDatabase implements Database {
  private file: any
  constructor() {
    this.file = this.read()
  }

  /**讀取JSON檔案 */
  private read(): any {
    let file = readFileSync(file_read)
    return JSON.parse(file.toString())
  }

  /**寫入JSON檔案 */
  private write(obj: Object): void {
    let json = JSON.stringify(obj, null, ' ')
    writeFileSync(file_write, json)
  }

  /**插入資料（須帶有id成員） */
  async insert<T extends { id: string }>(type: string, query: T): Promise<T> {
    if (!Array.isArray(this.file[type])) throw Error('type is not exits')
    if (query.id === undefined) query.id = this.hash(type)
    else for (let item of this.file[type]) {
      if (item.id === query.id) throw Error('id aready exist')
    }
    this.file[type].push(query)
    this.write(this.file)
    return query
  }

  /**更新資料為新物件 */
  async updateObj<T>(type: string, query: any, newObj: T): Promise<T[]> {
    let save = await this.get<T>(type, query)
    let result = await this.getItem(type, query)
    result.forEach((value: number) => {
      this.file[type][value] = newObj
    })
    this.write(this.file)
    return save
  }

  /*更新資料，僅更新指定部份 */
  async update<T>(type: string, query: any, newQuery: any): Promise<T[]> {
    let array: number[] = await this.getItem<T>(type, query)
    let result: T[] = await this.get<T>(type, query)
    for (let n of array) {
      for (let item in newQuery) {
        this.file[type][n][item] = newQuery[item]
      }
    }
    this.write(this.file)
    return result
  }

  /**查找，回傳index數組 */
  private async getItem<T>(type: string, query: any): Promise<number[]> {
    let result: number[] = new Array()
    if (!Array.isArray(this.file[type])) throw Error('type is not exist')
    let array = this.file[type]
    //查找資料庫的每一項
    for (let index=0; index<array.length; index++) {
      let isTrue = true
      //查找 query 的每一項，有一項失敗就離開，否則加入結果
      for (let item in query) {
        //如果是 $or 則改用 or 查詢，預設是失敗的，只要有一項配對就成功
        if (item === '$or') {
          isTrue = false
          for (let i in query.$or) {
            if (array[index][i] === query.$or[i]) {
              isTrue = true
              break
            }
          }
        }
        else if (query[item] !== array[index][item]) {
          isTrue = false
          break
        }
      }
      if (isTrue) result.push(index)
    }
    return result
  }

  /**查找，回傳內容數列 */
  async get<T>(type: string, query: any): Promise<T[]> {
    let array = await this.getItem(type, query)
    let result = new Array<T>()
    for (let i of array) {
      result.push(this.file[type][i])
    }
    return result
  }

  async getOne<T>(type: string, query: any): Promise<T> {
    let result: Array<T> = await this.get<T>(type, query)
    if (result.length === 0) throw Error('no ' + type + ' found')
    else if (result.length > 1) throw Error('many ' + type + ' found')
    else return result[0]
  }

  async delete<T>(type: string, query: any): Promise<T[]> {
    let array = await this.get<T>(type, query)
    if (!Array.isArray(this.file[type])) throw(Error('type is not exist'))
    for (let obj of array) {
      let index = array.indexOf(obj)
      this.file[type].splice(index)
    }
    this.write(this.file)
    return array
  }

  hash(type: string): string {
    return this.file[type].length.toString()
  }

}