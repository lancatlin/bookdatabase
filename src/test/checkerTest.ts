import { Checker, CheckerFactory } from "../model/checker";
import { Core, CoreFactory } from "../model/core";
import { Database, DatabaseFactory } from "../model/database";
import { Book } from "../model/book";
import { Member } from "../model/member";
const assert = require('assert')

const books = [
  new Book('test1', 'one', '13584999'),
  new Book('test1', 'gg', '1554778922'),
  new Book('two', 'llll', 'test1'),
  new Book('three')
]

async function all_test () {
  let db: Database = DatabaseFactory()
  let checker: Checker = CheckerFactory(db)
  let core: Core = CoreFactory(db)
  for (let b of books) await db.insert<Book>('book', b)
  let member: Member = await db.insert<Member>('member', new Member('lancat', '1655', new Date(), 0))
  let lend_list: Book[] = await checker.check_lend('test1')
  for (let b of lend_list) {
    await core.lend(b.id, member.id)
  }
  let return_list: Book[] = await checker.check_return('test1')
  for (let i=0; i<return_list.length; i++) {
    assert(return_list[i].status === 1 && return_list[i].id === lend_list[i].id) 
  }
  for (let b of return_list) {
    await core.returnBook(b.id)
  }
  assert((await checker.check_return('test1')).length === 0)
}

all_test().catch((err: Error) => {
  debugger
  console.log(err)
})