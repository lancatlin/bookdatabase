import { Book } from "../model/book";
import { DatabaseFactory, Database } from "../model/database";
import { Member } from "../model/member";
import { Core, Result, CoreFactory } from "../model/core";
import { Event } from "../model/event";
const assert = require('assert')

function lend () {
  let db: Database = DatabaseFactory()
  let book = new Book('test1')
  let member = new Member('member1', '5541', new Date(), 0)
  let core: Core = CoreFactory(db)
  let event: Event
  db.insert<Book>('book', book)
  .then((b: Book) => {
    return db.insert<Member>('member', member)
  })
  .then((m: Member) => core.lend(book.name, member.name))
  .then((result: Result) => {
    event = result.event
    return core.lend(book.name, member.idnumber)
  })
  .then((r: Result) => {
    debugger
    throw Error('should not lend the same book twice')
  })
  .catch((err) => {
    debugger
    db.delete<Book>('book', {id: book.id})
    db.delete<Member>('member', {id: member.id})
    db.delete<Event>('event', {id: event.id})
  })
}

function returning () {
  let db: Database = DatabaseFactory()
  let book = new Book('test1')
  let member = new Member('member1', '5541', new Date(), 0)
  let core: Core = CoreFactory(db)
  let event: Event
  db.insert<Book>('book', book)
  .then((b: Book) => db.insert<Member>('member', member))
  .then((m: Member) => {
    return core.lend(book.id, member.id)
  })
  .then((result: Result) => {
    event = result.event
    return core.returnBook(book.id) 
  })
  .then((result: Result) => {
    return db.get<Book>('book', {id: book.id})
  })
  .then((b: Book[]) => {
    assert(b[0].status === 0)
    return db.get<Member>('member', {id: member.id})
  })
  .then((m: Member[]) => {
    assert(m[0].status === 0)
  })
  .catch(err => {
    debugger
    console.log(err)
  })
}

function insert() {
  let db = DatabaseFactory()
  let book1 = new Book(db.hash('book'), 'test1', 'author', '0000')
  let book2 = new Book(db.hash('book'), 'test1', 'author', '0000')
  let member1 = new Member('one', '5544', new Date(), 0)
  let member2 = new Member('two', '1233', new Date(), 0)
  db.insert<Book>('book', book1)
  db.insert<Book>('book', book2)
  db.insert<Member>('member', member1)
  db.insert<Member>('member', member2)
}

function multireturn () {
  /**測試在多人同借一本ISBN相同書的情況下會發生什麼情形 */
  let db = DatabaseFactory()
  let core = CoreFactory(db)
  //lend the book to one
  core.lend('test1', 'one') //should be ok, system will choose one for it
  core.lend('test1', 'two') //still should be ok, system know there is only one book can be lending.
  /**
   * ask: How system know these two book is same?
   * by name? or by ISBN?
   * if no ISBN?
   */
  core.returnBook('test1') //should throw error, because it doesn't know that which event is it.
  core.returnBook('test1') //should work. because there is only one book its status is 1
}

function multireturnImpl () {
  let db: Database = DatabaseFactory()
  let core: Core = CoreFactory(db)
  debugger
  core.lend('test1', 'one')
  .then((result: Result) => {
    debugger
    console.log(result)
    return core.lend('test1', 'two')
  })
  .then(result => {
    console.log(result)
    return core.returnBook('test1')
  })
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
}

returning()