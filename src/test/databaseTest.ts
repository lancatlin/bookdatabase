import { DatabaseFactory, Database, } from "../model/database";
import { Book } from "../model/book";
import { Member } from "../model/member";

const assert = require('assert')

function insert () {
  let book = new Book('1533', 'LibreOffice')
  let database = DatabaseFactory()
  database.insert<Book>('book', book).then((book: Book) => {
    console.log(book)
  })
  .catch(err => {
    console.log(err)
  })
  /*
  let member = new Member('15688', '林宏信', '9666', new Date(), 0)
  database.insert<Member>(member)
  .then(member => {
    console.log(member)
  })
  .catch(err => console.log(err))
  */
}

function get () {
  let query = {
    name:  '鳥哥',
    id:  '15688'
  }
  let database: Database = DatabaseFactory()
  database.get<Member>('member', query).then((result: Member[]) => {
    console.log(result)
  })
}

function del (): void {
  let query = {
    id:  '1233'
  }
  let database: Database = DatabaseFactory()
  database.delete<Book>('book', query)
  database.delete<Member>('member', {id: '15688'})
}

function hash (): void {
  let book = new Book('1533', 'LibreOffice')
  let database = DatabaseFactory()
  console.log(database.hash('book'))
}

function _allTest () {
  let db: Database = DatabaseFactory()
  let book = new Book(db.hash('book'), 'test1')
  let book2 = new Book('1', 'test2')
  db.insert<Book>('book', book2)
  db.insert<Book>('book', book)
  .then((b: Book) => {
    debugger
    return db.get<Book>('book', {$or: {name: 'test1', id: '1'}})
  })
  .then((b: Book[]) => {
    debugger
    assert(b[0] === book)
    book.name = 'test2'
    return db.update<Book>('book', {id: book.id}, book)
  })
  .then((b: Book[]) => {
    assert(b[0].name === book.name)
    db.delete<Book>('book', {id: book.id})
    return db.get<Book>('book', {id: book.name})
  })
  .then((b: Book[]) => {
    assert(b.length === 0)
  })
  .catch((err: Error) => {
    debugger  
    throw err
  })
}

async function allTest () {
  let db: Database = DatabaseFactory()
  let books = [new Book('test1'), new Book('test2'), new Book('test3', 'lancatlin')]
  for (let i of books) await db.insert<Book>('book', i)
  let book1 = await db.getOne<Book>('book', {name: 'test1'})
  assert(book1.name === books[0].name && book1.id === '0')
  let book2 :Book = (await db.update<Book>('book', {id: book1.id}, {author: 'justin'}))[0]
  assert(book2.author === 'justin')
  await db.delete('book', {id: book1.id})
  try {
    //should throw error
    await db.getOne<Book>('book', {id: book1.id})
  }catch (err) {
    console.log(err.stack)
  }
}

//insert()
allTest().catch((err :Error) => {
  debugger
  console.log(err.stack)
})