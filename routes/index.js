var express = require('express');
var router = express.Router();

const db = require('../js/model/database').DatabaseFactory()
const core = require('../js/model/core').CoreFactory(db)
const checker = require('../js/model/checker').CheckerFactory(db)

/* GET home page. */ 
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* 各書籍頁面 */
router.get('/book/:id', (req, res, next) => {
  db.getOne('book', {id: req.params.id})
  .then(book => (
    res.render('book', book)
  ))
  .catch(err => res.render('error', {error: err, message: 'No Book Found'})) 
})

module.exports = router;
